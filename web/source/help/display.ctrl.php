<?php
/**
 * 帮助系统
 * [WeEngine System] Copyright (c) 2013 WE7.CC
 */
defined('IN_IA') or exit('Access Denied');
global $_W;

$_W['page']['title'] = '帮助系统';
template('help/display');